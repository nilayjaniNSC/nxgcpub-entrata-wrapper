import requests
import base64
import json
import logging
import logging.handlers
import sys
from Services import Services

userName = 'Nortek_api@lerner-test-17049'
password = 'Password1'
apiUrl = 'https://lerner-test-17049.entrata.com/api/v1/'

def getCustomerinfo(propertyId):
    #1123518 no record found 1035076

    customerDetails = Services(userName, password)
    customerJson = customerDetails.processApiService(apiUrl, 'customers', propertyId)

    # customerJson = json.loads(customerResponse.text)
    statusCode = customerJson['response']['code']

    try:
        info_customer = customerJson['response']['result']['Customers']['Customer']

        for cust_info in info_customer:
            print('Customer: ' + cust_info['FirstName'] + ' - ' + cust_info['Address'])
    except KeyError:
        print("Customer info not found for property id:" + propertyId)


def getPropertyInfo():

    propertiesDetails = Services(userName, password)
    propertyJson = propertiesDetails.processApiService(apiUrl, 'properties')

    # propertyJson = json.loads(propertiesResponse.text)

    try:
        statusCode = propertyJson['response']['code']

        if statusCode == 200:
            info_props = propertyJson['response']['result']['PhysicalProperty']['Property']

            for i_prop in info_props:
                propertyId = int(i_prop['PropertyID'])

                if propertyId == 1035076:
                    print("Property ID: " + str(i_prop['PropertyID']))
                    print("Address: " + str(i_prop['Address']))
                    print("MarketingName: " + str(i_prop['MarketingName']))
                    print("Customer Info---")
                    getCustomerinfo(propertyId)
                    print("-" * 50)
                    break
    except KeyError:
        print("Error: " + propertyJson['response']['error']['message'])


if __name__ == "__main__":

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    fmt = '%(asctime)s Entrata Services: %(levelname)s %(message)s'
    formatter = logging.Formatter(fmt)
    handler = logging.StreamHandler(stream=sys.stdout)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    getPropertyInfo()