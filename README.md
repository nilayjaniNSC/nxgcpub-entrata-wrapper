# Telephone Entry - Entrata API Intergration Wrapper #

This is not part of Telephone Entry project. 

### What is this repository for? ###

* Wrapper to API
* 0.00.01
* Provide functionalities to add/remove/update user(s).

### How do I get set up? ###

* Python venv
* * How to create vnev
* * link: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/

* Python How to activate venv
* * Activate venv
* * ```bash
    source venv/bin/active
* Python Package list
* * request
* * json
* Python How to export existing venv
* * ```bash 
    pip install -r requirements.txt
* * link: https://blog.usejournal.com/why-and-how-to-make-a-requirements-txt-f329c685181e 
* Required Version of Python: 3.x


### Contribution guidelines ###

* Python: Code Guidelines
* * link: https://www.python.org/dev/peps/pep-0008/ 


### Who do I talk to? ###

* Repo owner or admin : Nilay Jani
* Lead Dev: Rakesh Shetty