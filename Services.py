import requests
import base64
import json
import logging
import logging.handlers
import sys

class Services():
    def __init__(self, userName, password):
        self.userName = userName
        self.password = password
        self.apiUrl = ''

    def generateAuth(self):
        entrataCredentials = self.userName + ':' + self.password
        entrataCredentials = base64.b64encode(entrataCredentials.encode("ascii"))
        entrataCredentials = entrataCredentials.decode("ascii")

        auth = 'Basic ' + entrataCredentials
        return auth

    def generateJsonRequest(self, serviceName, propertyId=None):

        if serviceName == 'properties':
            jsonRequest = {
                "auth": {
                    "type": "basic"
                },
                "method": {
                    "name": "getProperties",
                    "version": "r1",
                    "params": {
                        "showAllStatus": "0"
                    }
                }
            }
        elif serviceName == 'customers':
            jsonRequest = {
                "auth": {
                    "type": "basic"
                },
                "method": {
                    "name": "getCustomers",
                    "version": "r1",
                    "params": {
                        "propertyId": propertyId,
                        "isAgreedToTermsOnly": "0"
                    }
                }
            }

        return jsonRequest

    def processApiService(self, apiUrl, serviceName, propertyId=None):
        auth = self.generateAuth()
        self.apiUrl = apiUrl + serviceName
        jsonRequest = self.generateJsonRequest(serviceName, propertyId)

        jsonRequest = json.dumps(jsonRequest, indent=4)
        payload = jsonRequest

        entrataHeaders = {
            "content-type": "application/json; charset=UTF-8",
            'Authorization': auth
        }

        try:
            # self.log.info("Entrata Properties API called, waiting for response...")
            response = requests.request("POST", self.apiUrl, auth=(self.userName, self.password),
                                        headers=entrataHeaders, data=payload,
                                        timeout=20)

            # self.log.info("API Response: %s", str(response.json()))
            json_r = json.loads(response.text)
            return json_r

        except requests.exceptions.HTTPError as errh:
            print("HTTP Error : " + str(errh))
        except requests.exceptions.ConnectionError as errc:
            print("Connection Error : " + str(errc))
        except requests.exceptions.Timeout as errt:
            print("Timeout Error : " + str(errt))
        except requests.exceptions.RequestException as err:
            print("RequestException Error " + str(err))