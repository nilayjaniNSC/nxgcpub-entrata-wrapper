#!/usr/bin/python3
import requests
import json

SERVICES = {
    "prop" : "properties",
}


resp = requests.post(
    "https://lerner-test-17049.entrata.com/api/v1/properties",
    auth=("Nortek_api@lerner-test-17049", "Password1"),
    headers={
        "content-type": "application/json; charset=UTF-8",
        "Authorization": "basic bm9ydGVrX2FwaUBsZXJuZXItdGVzdC0xNzA0OTpQYXNzd29yZDE="  # base64 username:password
    },
    data='''
        {
            "auth": {
                "type": "basic"
            },
            "requestId": 15,
            "method": {
                "name": "getProperties",
                "version" : "r1",
                "params" : {
                   "showAllStatus" : "0"
                }            
            }
        }
        '''
)

print(str(resp.status_code))
print(str(resp.url))
print(str(resp.headers['content-type']))
print(str(len(resp.content)))
# print(str(resp.text))

json_r = json.loads(resp.text)
info_props = json_r['response']['result']['PhysicalProperty']['Property']
print(str(type(info_props)))
print(len(info_props))
print(info_props)
for i_prop in info_props:
    prop_id = str(i_prop['PropertyID'])

    prop_name = str(i_prop['MarketingName']) if i_prop['MarketingName'] else "-No Name-"
    prop_web_site = str(i_prop['webSite']) if i_prop['webSite'] else "- No website-"
    prop_addr = str(i_prop['Address']) if i_prop['Address'] else "-No Address-"

    print("Propetry info:-")
    print(" id:" + prop_id)
    print(" MarketingName:" + prop_name)
    print(" webSite:" + prop_web_site)
    print(" Address:" + prop_addr)

    print("-"*50)


